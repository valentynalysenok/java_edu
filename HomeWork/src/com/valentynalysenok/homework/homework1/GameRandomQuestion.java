package com.valentynalysenok.homework.homework1;

import java.util.Arrays;
import java.util.Scanner;

public class GameRandomQuestion {

    public static void main(String[] args) {

        int[] array = new int[]{};
        int correctYear, year;

        Object[][] arr = new Object[][]{
                //0    1
                {1874, "When Winston Churchill was born?"}, //0
                {1878, "When Joseph Stalin was born?"},     //1
                {1889, "When Adolf Hitler was born?"},      //2
                {1914, "When did the World War I begin?"},  //3
                {1939, "When did the World War II begin?"}, //4
        };

        System.out.println("Let the game begin!");

        System.out.println("Please enter your name:");
        Scanner enterName = new Scanner(System.in);
        String nameUser = enterName.nextLine();

        //(int)(( Math.random() * (b - a + 1) + a)
        //[0,4] -> rows
        int a = (int) ( Math.random() * 5 ); //вариативность строки
        String question = (String) arr[a][1];
        System.out.println(question);       //рандомный вопрос
        correctYear = (int)arr[a][0];       //рандомный ответ

        do {
            System.out.println("\nPlease enter your answer:");
            Scanner answer = new Scanner(System.in);
            year = answer.nextInt();
            array = addNumberToArray(array, year);

            if (correctYear == year) {
                System.out.printf("It's correct! %s Nice job.", nameUser);
                System.out.println("\nYour previous answers are: " + sortArrayFromMinToMax(array));
                System.out.println("The correct one was: " + correctYear);
            } else {
                System.out.printf("\nYour answer is wrong, please try again. %s", question);
            }
        } while (correctYear != year);

    }
    //сортировка массива чисел
    public static String sortArrayFromMinToMax(int[] arr) {
        //Bubble Sort
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        return Arrays.toString(arr);
    }

    //добавляю числа, введенные пользователем в массив с ограничением ">0"
    public static int[] addNumberToArray(int[] arr, int number) {
        if (number < 0) {
            System.out.println("Your number should be greater than 0");
        } else {
            arr = Arrays.copyOf(arr, arr.length + 1);
            arr[arr.length - 1] = number;
        }
        return arr;
    }


}
