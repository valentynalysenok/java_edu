package com.valentynalysenok.homework.homework11;

import com.valentynalysenok.homework.homework11.Animals.DomesticCat;
import com.valentynalysenok.homework.homework11.Animals.Pet;
import com.valentynalysenok.homework.homework11.Human.Human;

import java.util.*;

public class FamilyHw11 {

    public static void main(String[] args) {

        Set<String> petHabits = new HashSet<>();
        petHabits.add("jumping");
        petHabits.add("scratching");
        petHabits.add("licking");
        petHabits.add("stealing food");
        petHabits.add("sleeping");

        Set<Pet> pets = new HashSet<>();
        DomesticCat cat = new DomesticCat("Kilka", 11, 89, petHabits);
        DomesticCat cat2 = new DomesticCat("Pushok", 8, 69, petHabits);
        DomesticCat cat3 = new DomesticCat("Birulka", 5, 92, petHabits);
        pets.add(cat);
        pets.add(cat2);
        pets.add(cat3);

        Map<String, String> motherSchedule = new HashMap<>();
        motherSchedule.put(DayOfWeek.MONDAY.day(), "Go to work");
        motherSchedule.put(DayOfWeek.TUESDAY.day(), "Go to work");
        motherSchedule.put(DayOfWeek.WEDNESDAY.day(), "Go to work");
        motherSchedule.put(DayOfWeek.THURSDAY.day(), "Go to work");
        motherSchedule.put(DayOfWeek.FRIDAY.day(), "Go to work");
        motherSchedule.put(DayOfWeek.SATURDAY.day(), "Go to the gym");
        motherSchedule.put(DayOfWeek.SUNDAY.day(), "Cleaning apartment");

        Map<String, String> fatherSchedule = new HashMap<>();
        fatherSchedule.put(DayOfWeek.MONDAY.day(), "Go to work");
        fatherSchedule.put(DayOfWeek.TUESDAY.day(), "Go to work");
        fatherSchedule.put(DayOfWeek.WEDNESDAY.day(), "Go to work");
        fatherSchedule.put(DayOfWeek.THURSDAY.day(), "Go to work");
        fatherSchedule.put(DayOfWeek.FRIDAY.day(), "Go to work");
        fatherSchedule.put(DayOfWeek.SATURDAY.day(), "Reading book");
        fatherSchedule.put(DayOfWeek.SUNDAY.day(), "Go to the gym");

        Human mother = new Human("Ella", "Tuck", 1980, 110, motherSchedule);
        Human father = new Human("Nick", "Tuck", 1980, 120, fatherSchedule);

        Family family = new Family(mother, father, pets);
        family.bornChild();

        System.out.println(family.countFamily());
        System.out.println(family);
        System.out.println(mother.feedPet(false));
        father.describePet();

    }
}
