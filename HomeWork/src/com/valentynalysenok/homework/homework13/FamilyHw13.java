package com.valentynalysenok.homework.homework13;

import com.valentynalysenok.homework.homework13.Animals.Dog;
import com.valentynalysenok.homework.homework13.Animals.DomesticCat;
import com.valentynalysenok.homework.homework13.Human.Human;

import java.util.*;

public class FamilyHw13 {
    public static void main(String[] args) {

        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        Set<String> petHabits = new HashSet<>();
        petHabits.add("scratching");
        petHabits.add("licking");
        petHabits.add("stealing food");
        petHabits.add("sleeping");

        DomesticCat cat = new DomesticCat("Kilka", 11, 89, petHabits);
        DomesticCat cat2 = new DomesticCat("Pushinka", 8, 69, petHabits);
        DomesticCat cat3 = new DomesticCat("Birulka", 5, 92, petHabits);
        Dog dog = new Dog("Bobik", 1, 79, petHabits);
        Dog dog2 = new Dog("Sharik", 4, 39, petHabits);

        Human mother1 = new Human("Ella", "Mask", 1976, 100);
        Human father1 = new Human("Nick", "Mask", 1970, 110);

        Human mother2 = new Human("Bella", "Trump", 1970, 150);
        Human father2 = new Human("Igor", "Trump", 1960, 90);

        Human mother3 = new Human("Mila", "Rockefeller", 1982, 118);
        Human father3 = new Human("Kevin", "Rockefeller", 1983, 111);

        Human mother4 = new Human("Ursula", "Hilton", 1990, 126);
        Human father4 = new Human("Piter", "Hilton", 1979, 121);

        Human child = new Human("Bella", "Jusk", 2002, 122);

        familyController.getFamilyService().createNewFamily(mother1, father1);
        familyController.getFamilyService().bornChild(familyDao.getFamilyByIndex(0), "Orest", "Lika");

        familyController.getFamilyService().createNewFamily(mother2, father2);
        familyController.getFamilyService().bornChild(familyDao.getFamilyByIndex(1), "Aragorn", "Aurel");

        familyController.getFamilyService().createNewFamily(mother3, father3);
        familyController.getFamilyService().bornChild(familyDao.getFamilyByIndex(2), "Gimly", "Georgina");

        familyController.getFamilyService().createNewFamily(mother4, father4);
        familyController.getFamilyService().bornChild(familyDao.getFamilyByIndex(3), "Julian", "Paola");
        familyController.getFamilyService().adoptChild(familyDao.getFamilyByIndex(3), child);

        System.out.println("\ngetFamiliesBiggerThan:");
        System.out.println(familyController.getFamilyService().getFamiliesBiggerThan(3));
        System.out.println("\ngetFamiliesLessThan:");
        System.out.println(familyController.getFamilyService().getFamiliesLessThan(3));
        System.out.println("\ncountFamiliesWithMemberNumber:");
        System.out.println(familyController.getFamilyService().countFamiliesWithMemberNumber(3));
        System.out.println("\ndeleteAllChildrenOlderThen:");
        System.out.println(familyController.getFamilyService().deleteAllChildrenOlderThen(16));
        familyController.getFamilyService().adoptChild(familyDao.getFamilyByIndex(3), child);
        System.out.println("\ncount:");
        System.out.println(familyController.getFamilyService().count());
        System.out.println("\naddPet:");
        System.out.println(familyController.getFamilyService().addPet(0, cat));
        System.out.println(familyController.getFamilyService().addPet(0, dog));
        System.out.println(familyController.getFamilyService().addPet(1, cat2));
        System.out.println(familyController.getFamilyService().addPet(2, cat3));
        System.out.println(familyController.getFamilyService().addPet(3, dog2));
        System.out.println("\ngetPets:");
        System.out.println(familyController.getFamilyService().getPets(0));
        System.out.println("\ndisplayAllFamilies:");
        familyController.getFamilyService().displayAllFamilies();


    }
}
