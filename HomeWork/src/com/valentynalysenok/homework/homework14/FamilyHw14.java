package com.valentynalysenok.homework.homework14;

import com.valentynalysenok.homework.homework14.Human.Human;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class FamilyHw14 {
    public static void main(String[] args) throws ParseException {

        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Ursula", "Hilton", 711061200000L, 126);
        Human father = new Human("Piter", "Hilton",711061200000L, 121);

        Human child = new Human("Bella", "Jusk", "20/05/2002", 122);

        System.out.println(mother.describeAge());
        System.out.println(child.describeAge());
        familyController.getFamilyService().createNewFamily(mother, father);
        familyController.getFamilyService().adoptChild(familyDao.getFamilyByIndex(0), child);
        familyController.getFamilyService().bornChild(familyDao.getFamilyByIndex(0), "Kevin", "Mila");
        familyController.getFamilyService().displayAllFamilies();
        familyController.deleteAllChildrenOlderThen(16);
        familyController.getFamilyService().displayAllFamilies();
    }
}
