package com.valentynalysenok.homework.homework14;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.Temporal;
import java.util.Date;

public class Time {
    public static void main(String[] args) throws ParseException {

        long birthDate = new Date(1992, 7, 14).getTime();

        String dateOfBirth = "14/07/1992";
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        Date dateNow = new Date();
        Date dateBirth = format.parse(dateOfBirth);

        long diff = dateNow.getTime() - birthDate;
        System.out.println(dateBirth.getTime());
        // Перевод количества дней между датами из миллисекунд в дни
        System.out.println(birthDate);
        int days =  (int)(diff / (24 * 60 * 60 * 1000)); // миллисекунды / (24ч * 60мин * 60сек * 1000мс)

        System.out.println(days);

    }

    public String describeAge(long birthDate) {
        String describeAge;
        long diff = new Date().getTime() - birthDate;
        int days =  (int)(diff / (24 * 60 * 60 * 1000));
        int years = days / 365;
        int month = years * 12;
        describeAge = years + " years " + month + " month " + days + " days";
        return describeAge;
    }

}
