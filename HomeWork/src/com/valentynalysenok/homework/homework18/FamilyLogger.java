package com.valentynalysenok.homework.homework18;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FamilyLogger {

    private static String logFile = "application.log";
    private final static DateFormat df = new SimpleDateFormat("dd/MM/yyyy  hh:mm ");

    public void info(String msg) {
        try {
            Date now = new Date();
            String currentTime = FamilyLogger.df.format(now);
            FileWriter aWriter = new FileWriter(logFile, true);
            aWriter.write(currentTime + " [DEBUG] " + msg
                    + System.getProperty("line.separator"));
            aWriter.flush();
            aWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void error(String msg) {
        try {
            Date now = new Date();
            String currentTime = FamilyLogger.df.format(now);
            FileWriter aWriter = new FileWriter(logFile, true);
            aWriter.write(currentTime + " [ERROR] " + msg
                    + System.getProperty("line.separator"));
            aWriter.flush();
            aWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
