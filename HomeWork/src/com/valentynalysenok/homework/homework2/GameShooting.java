package com.valentynalysenok.homework.homework2;

import java.util.Scanner;

public class GameShooting {

    public static void main(String[] args) {

        System.out.println("All set. Get ready to rumble!.");

        Scanner userAnswer = new Scanner(System.in);

        //(int)(( Math.random() * (b - a + 1) + a)
        //[1,5] -> rows
        int randomRow = (int) (Math.random() * 5 + 1);
        //[1,5] -> columns
        int randomCol = (int) (Math.random() * 5 + 1);
        System.out.println("[" + randomRow + "]" + "[" + randomCol + "]"); //подсказка цели

        System.out.println("Please enter the row number from 1 to 5:");
        int rowNum = userAnswer.nextInt();
        while (rowNum < 1 || rowNum > 5) {
            System.out.println("Please try again, your number should be in range [1, 5]: ");
            rowNum = userAnswer.nextInt();
        }
        System.out.println("Please enter the column number from 1 to 5:");
        int colNum = userAnswer.nextInt();
        while (colNum < 1 || colNum > 5) {
            System.out.println("Please try again, your number should be in range [1, 5]: ");
            colNum = userAnswer.nextInt();
        }

        Object[][] arr = new Object[6][6];

        boolean flag = true;

        do {
            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < arr.length; j++) {

                    if (i == 0 && j == 0) {
                        arr[i][j] = 0;
                    } else if (i == 0 && j >= 1 && j <= 5) {
                        arr[i][j] = j;
                    } else if (j == 0 && i >= 1 && i <= 5) {
                        arr[i][j] = i;
                    } else if (randomRow == rowNum && randomCol == colNum) {
                        arr[rowNum][colNum] = "x";
                    } else if (i == rowNum && j == colNum) {
                        arr[i][j] = "*";
                    } else if (arr[i][j] == null) {
                        arr[i][j] = "-";
                    }
                    System.out.print(arr[i][j] + " | ");
                }
                System.out.println();
            }
            if (arr[rowNum][colNum] == "x") {
                break;
            }
            System.out.println("Please enter the row number from 1 to 5:");
            rowNum = userAnswer.nextInt();
            while (rowNum < 1 || rowNum > 5) {
                System.out.println("Please try again, your number should be in range [1, 5]: ");
                rowNum = userAnswer.nextInt();
            }
            System.out.println("Please enter the column number from 1 to 5:");
            colNum = userAnswer.nextInt();
            while (colNum < 1 || colNum > 5) {
                System.out.println("Please try again, your number should be in range [1, 5]: ");
                colNum = userAnswer.nextInt();
            }
        } while (flag);

        System.out.println("You have won!");

    }

}
