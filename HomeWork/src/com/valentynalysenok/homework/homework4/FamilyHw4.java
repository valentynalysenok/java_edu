package com.valentynalysenok.homework.homework4;

public class FamilyHw4 {
    public static void main(String[] args) {
        Pet dog = new Pet();
        dog.species = "dog";
        dog.nickname = "Ronny";
        dog.age = 13;
        dog.trickLevel = dog.testTrickLevel(47);
        dog.habits = new String[]{"barking", "chewing", "scratching", "chasing it tail", "burying food", "digging"};
        System.out.println(dog.toString());
        dog.eat();
        dog.respond();
        dog.foul();

        System.out.println();

        Human mother = new Human();
        mother.name = "Ember";
        mother.surname = "Guver";

        Human father = new Human();
        father.name = "Richard";
        father.surname = "Guver";

        Human daughter = new Human();
        daughter.name = "Erica";
        daughter.surname = "Guver";
        daughter.year = 1989;
        daughter.iq = 113;
        daughter.arrSchedule = new String[][]{
                {"Saturday", "Go to the gym. Shopping food. Reading book."},
                {"Sunday", "Cleaning apartment. Take a rest."}
        };
        daughter.mother = mother;
        daughter.father = father;
        daughter.pet = dog;
        System.out.println(daughter.toString());
        daughter.greetPet();
        System.out.println(daughter.describePet());
        daughter.feedPet(false);
    }
}



