package com.valentynalysenok.homework.homework4;

import java.util.Arrays;
import java.util.Random;

class Human {
    String name;
    String surname;
    int year;
    int iq;
    Human mother;
    Human father;
    Pet pet;

    String[][] arrSchedule = new String[][]{};

    public void greetPet() {
        System.out.println("Hi, " + this.pet.nickname);
    }

    public String describePet() {
        String petTricks;
        if (pet.trickLevel <= 50 && pet.trickLevel > 0) {
            petTricks = "almost not tricky";
        } else if (pet.trickLevel > 50) {
            petTricks = "very tricky";
        } else {
            petTricks = "almost dead";
        }
        return "I have " + pet.species + ", it's " + pet.age + " years, it's " + petTricks + ".";
    }

    //не обязательное задание продвинутой сложности:
    public boolean feedPet(boolean isTimeToFeedPet) {
        Random random = new Random();
        int randomLevel = random.nextInt(100);
        System.out.println(randomLevel);
        if (isTimeToFeedPet == true) {
            System.out.println("Hmm...I'll feed the " + pet.nickname);
        } else {
            if (pet.trickLevel > randomLevel) {
                System.out.println("Hmm...I'll feed the " + pet.nickname);
            } else {
                System.out.println("I think, " + pet.nickname + " don't hungry.");
            }
        }
        if (pet.trickLevel > randomLevel || isTimeToFeedPet == true) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        String aboutMyself = "";
        aboutMyself += "Human { ";
        aboutMyself += "name: " + name;
        aboutMyself += ", surname: " + surname;
        aboutMyself += ", year: " + year;
        aboutMyself += ", iq: " + iq;
        aboutMyself += ";\nMy schedule on weekends: " + Arrays.toString(arrSchedule[0])
                + ", " + Arrays.toString(arrSchedule[1]);

        if ((mother != null)) {
            aboutMyself += ";\nMother: " + mother.name + " " + mother.surname;
        } else {
            aboutMyself += ";\nMother: " + null;
        }
        if (this.father != null) {
            aboutMyself += ", Father: " + father.name + " " + father.surname;
        } else {
            aboutMyself += ", Father: " + null;
        }

        aboutMyself += ";\nPet: " + pet + "}";
        return aboutMyself;

    }
}
