package com.valentynalysenok.homework.homework5;

import java.util.Arrays;
import java.util.Random;

class Human {

    String name;
    String surname;
    int year;
    int iq;
    Human mother;
    Human father;
    Pet pet;

    String[][] arrSchedule = new String[][]{};

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.father = null;
        this.mother = null;
    }

    //конструктор, описывающий имя, фамилию и год рождения
    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    //конструктор, описывающий имя, фамилию, год рождения, папу и маму
    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    //конструктор, описывающий все поля
    public Human(String name, String surname, int year, int iq, Human mother, Human father, Pet pet, String[][] arrSchedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mother = mother;
        this.father = father;
        this.pet = pet;
        this.arrSchedule = arrSchedule;
    }

    ////пустой конструктор
    public Human() {
    }

    public void greetPet() {
        System.out.println("Hi, " + this.pet.nickname);
    }

    public String describePet() {
        String petTricks;
        if (this.pet.trickLevel <= 50 && this.pet.trickLevel > 0) {
            petTricks = "almost not tricky";
        } else if (this.pet.trickLevel > 50) {
            petTricks = "very tricky";
        } else {
            petTricks = "very clever and very tricky";
        }

        if (this.pet.age == 0) {
            return "I have " + this.pet.species + ", it's " + petTricks + ".";
        } else {
            return "I have " + this.pet.species + ", it's " + this.pet.age + " years, it " + petTricks + ".";
        }
    }

    public boolean feedPet(boolean isTimeToFeedPet) {
        Random random = new Random();
        int randomLevel = random.nextInt(100);
        System.out.println(randomLevel);
        if (isTimeToFeedPet == true) {
            System.out.println("Hmm...I'll feed the " + this.pet.nickname);
        } else {
            if (this.pet.trickLevel > randomLevel) {
                System.out.println("Hmm...I'll feed the " + this.pet.nickname);
            } else {
                System.out.println("I think, " + this.pet.nickname + " don't hungry.");
            }
        }
        if (this.pet.trickLevel > randomLevel || isTimeToFeedPet == true) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public String toString() {
        String aboutMyself = "";
        aboutMyself += "Human {";
        aboutMyself += "name: " + this.name;
        aboutMyself += ", surname: " + this.surname;
        aboutMyself += ", year: " + this.year;

        if (this.iq == 0) {
            aboutMyself += "}";
        } else {
            aboutMyself += ", iq: " + this.iq + "}";
        }

        if (this.arrSchedule.length == 0) {
            aboutMyself += "";
        } else {
            aboutMyself += ";\nMy schedule on weekends: " + Arrays.toString(arrSchedule[0])
                    + ", " + Arrays.toString(arrSchedule[1]);
        }

        if (this.mother != null) {
            aboutMyself += ";\nMother: " + this.mother.name + " " + this.mother.surname;
        } else {
            aboutMyself += ";\nMother: " + null;
        }
        if (this.father != null) {
            aboutMyself += ", Father: " + this.father.name + " " + this.father.surname;
        } else {
            aboutMyself += ", Father: " + null;
        }

        if (this.pet == null) {
        } else {
            aboutMyself += ";\nPet: " + this.pet + " }";
        }
        return aboutMyself;
    }

}
