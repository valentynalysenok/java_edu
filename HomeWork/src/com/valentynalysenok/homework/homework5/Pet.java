package com.valentynalysenok.homework.homework5;

import java.util.Arrays;

class Pet {
    String species;
    String nickname;
    int age;
    int trickLevel;
    String[] habits = new String[]{};

    //конструктор, описывающий вид животного и его кличку
    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    //конструктор, описывающий все поля животного
    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = testTrickLevel(trickLevel);
        this.habits = habits;
    }

    //пустой конструктор
    public Pet() {
    }

    //целое число от 0 до 100
    private int testTrickLevel(int level) {
        if (level < 0) {
            this.trickLevel = 0;
        } else if (level > 100) {
            this.trickLevel = 100;
        } else {
            this.trickLevel = level;
        }
        return this.trickLevel;
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {

        String aboutPet = "";
        aboutPet += this.species + " { ";
        aboutPet += "nickname: " + this.nickname;
        if (this.age == 0) {
        } else {
            aboutPet += ", age: " + this.age;
        }
        if (this.trickLevel == 0) {
        } else {
            aboutPet += ", trickLevel: " + this.trickLevel;
        }
        if (this.habits.length == 0) {
            aboutPet += " } ";
        } else {
            aboutPet += ", habits: " + Arrays.toString(this.habits) + " }";
        }

        return aboutPet;
    }

}

