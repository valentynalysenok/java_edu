package com.valentynalysenok.homework.homework6;

public class FamilyHw6 {
    public static void main(String[] args) {

        String[] petHabits = new String[]{"jumping", "scratching", "licking", "stealing food", "sleeping"};
        String[][] motherShedule = new String[][]{
                {"Saturday", "Go to the gym"},
                {"Sunday", "Cleaning apartment"}
        };
        String[][] fatherSchedule = new String[][]{
                {"Saturday", "Reading book"},
                {"Sunday", "Go to the gym"}
        };
        String[][] childrenSchedule = new String[][]{
                {"Saturday", "Take a rest"},
                {"Sunday", "Preparing for school"}
        };

        Human mother = new Human("Ella", "Tuck", 1980, 123, motherShedule);
        Human father = new Human("Nick", "Tuck", 1980, 123, fatherSchedule);

        Pet pet = new Pet("cat", "Kilka", 11, 89, petHabits);

        Family family = new Family(mother, father, pet);
        family.addChild(new Human("Boris", "Tuck", 2005, 111, childrenSchedule));
        family.addChild(new Human("Lily", "Tuck", 2007, 100, childrenSchedule));

        family.deleteChild(1);
        System.out.println(family.countFamily());
        System.out.println(family.toString());
    }
}
