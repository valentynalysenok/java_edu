package com.valentynalysenok.homework.homework8;

public class FamilyHw8 {

    public static void main(String[] args) {

        String[] petHabits = new String[]{"jumping", "scratching", "licking", "stealing food", "sleeping"};
        String[][] motherSchedule = new String[][]{
                {DayOfWeek.MONDAY.day(), "Go to work"},
                {DayOfWeek.TUESDAY.day(), "Go to work"},
                {DayOfWeek.WEDNESDAY.day(), "Go to work"},
                {DayOfWeek.THURSDAY.day(), "Go to work"},
                {DayOfWeek.FRIDAY.day(), "Go to work"},
                {DayOfWeek.SATURDAY.day(), "Go to the gym"},
                {DayOfWeek.SUNDAY.day(), "Cleaning apartment"}
        };
        String[][] fatherSchedule = new String[][]{
                {DayOfWeek.MONDAY.day(), "Go to work"},
                {DayOfWeek.TUESDAY.day(), "Go to work"},
                {DayOfWeek.WEDNESDAY.day(), "Go to work"},
                {DayOfWeek.THURSDAY.day(), "Go to work"},
                {DayOfWeek.FRIDAY.day(), "Go to work"},
                {DayOfWeek.SATURDAY.day(), "Reading book"},
                {DayOfWeek.SUNDAY.day(), "Go to the gym"}
        };
        String[][] childrenSchedule = new String[][] {
                {DayOfWeek.MONDAY.day(), "Go to school"},
                {DayOfWeek.TUESDAY.day(), "Go to school"},
                {DayOfWeek.WEDNESDAY.day(), "Go to school"},
                {DayOfWeek.THURSDAY.day(), "Go to school"},
                {DayOfWeek.FRIDAY.day(), "Go to school"},
                {DayOfWeek.SATURDAY.day(), "Take a rest"},
                {DayOfWeek.SUNDAY.day(), "Preparing to school"}
        };

        Human mother = new Human("Ella", "Tuck", 1980, 123, motherSchedule);
        Human father = new Human("Nick", "Tuck", 1980, 123, fatherSchedule);

        Pet pet = new Pet(Species.CAT, "Kilka", 11, 89, petHabits);

        Family family = new Family(mother, father, pet);
        family.addChild(new Human("Boris", "Tuck", 2005, 111, childrenSchedule));
        family.addChild(new Human("Lily", "Tuck", 2007, 100, childrenSchedule));

        family.deleteChild(0);
        System.out.println(family.countFamily());
        System.out.println(family.toString());

    }
}
