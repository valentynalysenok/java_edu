package com.valentynalysenok.homework.homework9;

import java.util.Arrays;
import java.util.Objects;

public class Family {

    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        if (mother != null && father != null) {
            this.mother = mother;
            this.father = father;
        }
        mother.setFamily(this);
        father.setFamily(this);
        this.children = new Human[0];
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        this.children = Arrays.copyOf(this.children, this.children.length + 1);
        this.children[this.children.length - 1] = child;
        //для того, чтобы получить у ребенка инфу о его семье - мы сможем это сделать по ссылке child.setFamily(this);
        child.setFamily(this);
    }

    public boolean deleteChild(int index) {
        int i;
        for (i = 0; i <= this.children.length; i++) {
            if (i == index) {
                break;
            }
        }
        for (int j = i; j < this.children.length - 1; j++) {
            this.children[j] = this.children[j + 1];
        }
        this.children = Arrays.copyOf(this.children, this.children.length - 1);
        return true;
    }

    public boolean deleteChild(Human human) {
        int i;
        for (i = 0; i <= this.children.length; i++) {
            if (children[i].equals(human)) {
                break;
            }
        }
        for (int j = i; j < this.children.length - 1; j++) {
            this.children[j] = this.children[j + 1];
        }
        this.children = Arrays.copyOf(this.children, this.children.length - 1);
        return true;
    }

    public int countFamily() {
        int sum = 0;
        sum += this.children.length;
        if (this.mother != null && this.father != null) {
            sum += 2;
        } else if (this.mother == null || this.father == null) {
            sum += 1;
        } else {
            sum += 0;
        }
        return sum;
    }

    @Override
    public String toString() {
        String aboutFamily = "";
        aboutFamily += "Family {";

        if (this.mother != null) {
            aboutFamily += "\nMother: " + this.mother;
        } else {
            aboutFamily += "Mother: " + null;
        }
        if (this.father != null) {
            aboutFamily += ",\nFather: " + this.father;
        } else {
            aboutFamily += ", Father: " + null + ", ";
        }

        aboutFamily += ",\nChildren: " + Arrays.toString(this.children);

        if (this.pet == null) {
        } else {
            aboutFamily += ";\nPet: " + this.pet + "}";
        }
        return aboutFamily;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return  Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father);
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("CLEAN: " + this.hashCode());
    }
}
