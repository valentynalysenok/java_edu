package com.valentynalysenok.homework.loginLogic;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class LoginForStep {

    public static void main(String[] args) throws IOException {
        Map<String, String> accountInfo = new HashMap<>();
        accountInfo.put("tina.lysenok@gmail.com", "Abrakadabra");
        accountInfo.put("tina.lysenok@gmail2.com", "Abrakadabra2");
        checkUserLoginPassword(accountInfo);
    }

    public static void checkUserLoginPassword(Map<String, String> accountInfo) throws IOException {
        BufferedReader stream = new BufferedReader(new InputStreamReader(System.in));
        AtomicReference<String> result = new AtomicReference<>("unsuccessful");

        while (result.get() == "unsuccessful") {
            String userLogin, userPassword;
            System.out.println("Enter Login:");
            userLogin = stream.readLine();
            System.out.println("Enter Password:");
            userPassword = stream.readLine();
            accountInfo.forEach((user, pass) -> {
                if (userLogin.equals(user) && userPassword.equals(pass)) {
                    System.out.println("You have login!");
                    result.set("successful");
                } else if (!userLogin.equals(user) && userPassword.equals(pass)) {
                    System.out.println("You haven't login!");
                }
            });
        }
    }
}
