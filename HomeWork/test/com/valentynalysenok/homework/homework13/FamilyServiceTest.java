package com.valentynalysenok.homework.homework13;

import com.valentynalysenok.homework.homework13.Animals.Dog;
import com.valentynalysenok.homework.homework13.Animals.DomesticCat;
import com.valentynalysenok.homework.homework13.Animals.Pet;
import com.valentynalysenok.homework.homework13.Human.Human;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class FamilyServiceTest {

    @Test
    public void displayAllFamiliesShouldReturnOutToStringFamiliesResult() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        String expectedResult = families.toString();
        String result = "[\nFamily {\nMother: \nHuman{name: null, surname: null, year: 0, iq: 0}" +
                "\nMy schedule: none\nFather: \nHuman{name: null, surname: null, year: 0, iq: 0}" +
                "\nMy schedule: none\nChildren: []]";
        //then
        assertEquals(expectedResult, result);
    }

    @Test
    public void getFamiliesBiggerThanShouldReturnFamiliesWithMembersMoreThanIndexParameter() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        List<Family> expectedResultTrue = families;
        List<Family> resultTrue = familyService.getFamiliesBiggerThan(1);
        List<Family> expectedResultFalse = null;
        List<Family> resultFalse = familyService.getFamiliesBiggerThan(3);
        //then
        assertEquals(expectedResultTrue, resultTrue);
        assertNotEquals(expectedResultTrue, resultFalse);
        assertEquals(expectedResultFalse, resultFalse);
        assertNotEquals(expectedResultFalse, resultTrue);
    }

    @Test
    public void getFamiliesLessThanShouldReturnFamiliesWithMembersLessThanIndexParameter() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        List<Family> expectedResultTrue = families;
        List<Family> resultTrue = familyService.getFamiliesLessThan(3);
        List<Family> expectedResultFalse = null;
        List<Family> resultFalse = familyService.getFamiliesLessThan(2);
        //then
        assertEquals(expectedResultTrue, resultTrue);
        assertNotEquals(expectedResultTrue, resultFalse);
        //assertEquals(expectedResultFalse, resultFalse);
        assertNotEquals(expectedResultFalse, resultTrue);
    }

    @Test
    public void countFamiliesWithMemberNumberShouldReturnNumberOfFamiliesWhereIndexEqualsToFamilyMembers() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        int expectedResultTrue = 1;
        int resultTrue = familyService.countFamiliesWithMemberNumber(2);
        int expectedResultFalse = 0;
        int resultFalse = familyService.countFamiliesWithMemberNumber(4);
        //then
        assertEquals(expectedResultTrue, resultTrue);
        assertNotEquals(expectedResultTrue, resultFalse);
        assertEquals(expectedResultFalse, resultFalse);
        assertNotEquals(expectedResultFalse, resultTrue);
    }

    @Test
    public void createNewFamilyShouldCreateAndReturnNewFamilyFromFamiliesList() {
        //given
        Human mother = new Human();
        Human father = new Human();
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        //when
        Family expectedResult = new Family(mother, father);
        Family result = familyService.createNewFamily(mother, father);
        int expectedResultFamilyInList = 1;
        int resultFamilyInList = families.size();
        new Family(mother, father);
        familyService.createNewFamily(mother, father);
        int resultFamilyInList2 = families.size();
        //then
        assertEquals(expectedResult, result);
        assertEquals(expectedResultFamilyInList, resultFamilyInList);
        assertNotEquals(expectedResultFamilyInList, resultFamilyInList2);
    }

    @Test
    public void bornChildShouldCreateNewHumanObjectAndReturnUpdatedFamily() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        Family expectedResult = family;
        Family result = familyService.bornChild(family, "Iggi", "Olga");
        //then
        assertEquals(expectedResult, result);
        assertTrue(expectedResult.getChildren().get(0).getName(),
                expectedResult.getChildren().get(0).getName() == "Iggi"
                || expectedResult.getChildren().get(0).getName() == "Olga");

    }

    @Test
    public void adoptChildShouldAddAndSaveChildInFamilyAndReturnFamilyObject() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Human child = new Human();
        Family family = new Family(mother, father);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        Family expectedResult = family;
        Family result = familyService.adoptChild(family, child);
        int expectedResultMembers = 3;
        int resultMembers = family.countFamily();
        //then
        assertEquals(expectedResult, result);
        assertEquals(expectedResultMembers, resultMembers);
        assertTrue(family.getChildren().get(0).getSurname(),
                family.getChildren().get(0).getSurname() == family.getFather().getSurname());
    }

    @Test
    public void deleteAllChildrenOlderThenShouldDeleteEldestChildrenFromFamilyAndReturnFamilyObject() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        Human child1 = new Human(null, null, 2010, 134);
        Human child2 = new Human(null, null, 1998, 124);
        Human child3 = new Human(null, null, 1992, 114);
        Human child4 = new Human(null, null, 2002, 119);
        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        List<Family> expectedResultTrue = families;
        List<Family> resultTrue = familyService.deleteAllChildrenOlderThen(18);
        int expectedResultSize = 4;
        int resultSize = family.countFamily();
        //then
        assertEquals(expectedResultTrue, resultTrue);
        //assertEquals(expectedResultFalse, resultFalse);
        assertEquals(expectedResultSize, resultSize);
    }

    @Test
    public void countShouldCountFamilyMembersQuantityAndReturnMembersValue() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        int expectedResult = 1;
        int result = familyService.count();
        familyService.getFamilyDao().deleteFamily(family);
        int expectedResult2 = 0;
        int result2 = familyService.count();
        //then
        assertEquals(expectedResult, result);
        assertEquals(expectedResult2, result2);
    }

    @Test
    public void getPetsShouldReturnSetOfPetsWithFamilyIndexInFamiliesList() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Dog dog = new Dog();
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        Family family = new Family(mother, father, pets);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        Set<Pet> expectedResult = pets;
        Set<Pet> result = familyService.getPets(0);
        Set<Pet> expectedResultFalse = null;
        Set<Pet> resultFalse1 = familyService.getPets(1);
        Set<Pet> resultFalse2 = familyService.getPets(-1);
        //then
        assertEquals(expectedResult, result);
        assertNotEquals(expectedResult, resultFalse1);
        assertEquals(expectedResultFalse, resultFalse1);
        assertEquals(expectedResultFalse, resultFalse2);
    }

    @Test
    public void addPetShouldAddAndSaveNewPetInSetOfPetsAndReturnFamilyObject() {
        Human mother = new Human();
        Human father = new Human();
        Dog dog = new Dog();
        DomesticCat cat = new DomesticCat();
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        Family family = new Family(mother, father, pets);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        Family expectedResultTrue = family;
        Family resultTrue = familyService.addPet(0, cat);
        Family expectedResultFalse = null;
        Family resultFalse = familyService.addPet(1, cat);
        //then
        assertEquals(expectedResultTrue, resultTrue);
        assertNotEquals(expectedResultTrue, resultFalse);
        assertEquals(expectedResultFalse, resultFalse);
        assertNotEquals(expectedResultFalse, resultTrue);
    }

}
