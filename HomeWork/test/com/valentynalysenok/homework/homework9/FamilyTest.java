package com.valentynalysenok.homework.homework9;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;

public class FamilyTest {

    @Test
    public void shouldReturnCorrectToStringValueForFamilyClass() {
        //given
        Human mother = new Human("Ella", "Tuck", 1980, 123);
        Human father = new Human("Nick", "Tuck", 1980, 123);
        Family family = new Family(mother, father);
        Human child = new Human("Boris", "Tuck", 2005, 111);
        family.addChild(child);
        //when
        String expectedResult = "Family {\n" +
                "Mother: \n" +
                "Human{name: Ella, surname: Tuck, year: 1980, iq: 123}," +
                "\nFather: \n" +
                "Human{name: Nick, surname: Tuck, year: 1980, iq: 123}," +
                "\nChildren: [\n" +
                "Human{name: Boris, surname: Tuck, year: 2005, iq: 111}]";
        //then
        String result = new String();
        result += "Family {";
        if (family.getMother() != null) {
            result += "\nMother: " + family.getMother();
        } else {
            result += "Mother: " + null;
        }
        if (family.getFather() != null) {
            result += ",\nFather: " + family.getFather();
        } else {
            result += ", Father: " + null + ", ";
        }
        result += ",\nChildren: " + Arrays.toString(family.getChildren());
        assertEquals(expectedResult, result);
    }

    @Test
    public void testEqualsAndHashCodeForFamilyClass() {
        //given
        Human mother1 = new Human("Margaret", "Baggins", 1925, 123);
        Human father1 = new Human("Bob", "Baggins", 1926, 127);
        Family family1 = new Family(mother1, father1);
        Human mother2 = new Human("Margaret", "Baggins", 1925, 123);
        Human father2 = new Human("Bob", "Baggins", 1926, 127);
        Family family2 = new Family(mother2, father2);
        Human mother3 = new Human("Melanie", "Griffin", 1985, 110);
        Human father3 = new Human("Piter", "Griffin", 1986, 87);
        Family family3 = new Family(mother3, father3);
        //when
        //then
        assertEquals(family1, family2);
        assertNotEquals(family1, family3);
        assertNotEquals(family2, family3);
        assertTrue(family1.hashCode() == family2.hashCode());
        assertFalse(family1.hashCode() == family3.hashCode());
        assertFalse(family2.hashCode() == family3.hashCode());
    }

    @Test
    public void shouldAddChildToTheFamily() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        Human child = new Human();
        //when
        family.addChild(child);
        int result = family.getChildren().length;
        //then
        //assertThat(family.getChildren().length, is(1));
        assertThat(result > 0);
    }

    @Test
    public void shouldDeleteChildFromFamilyByIndexInArray() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        Human child = new Human();
        //when
        family.addChild(child);
        family.deleteChild(0);
        int result = family.getChildren().length;
        int expectedResult = 0;
        //then
        assertThat(result, is(expectedResult));
    }

    @Test
    public void shouldDeleteChildFromFamilyByHumanObj() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        Human child = new Human();
        //when
        family.addChild(child);
        family.deleteChild(child);
        int result = family.getChildren().length;
        int expectedResult = 0;
        //then
        assertThat(result, is(expectedResult));
    }

    @Test
    public void shouldCountQuantityOfFamilyMembers() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        Human child = new Human();
        //when
        family.addChild(child);
        int countExpectedResult = 3;
        int countResult = family.countFamily();
        //then
        assertThat(countResult, is(countExpectedResult));
    }

}
