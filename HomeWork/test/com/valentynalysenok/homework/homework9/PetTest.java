package com.valentynalysenok.homework.homework9;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class PetTest {

    @Test
    public void shouldReturnCorrectToStringValueForPet() {
        //given
        String[] petHabits = new String[]{"jumping", "scratching", "licking", "stealing food", "sleeping"};
        Pet pet = new Pet(Species.CAT, "Kilka", 11, 89, petHabits);
        //when
        String expectedResult = "cat {nickname: Kilka, age: 11, it can fly: false, it has fur: true, " +
                "it has 4 legs, trickLevel: 89, habits: [jumping, scratching, " +
                "licking, stealing food, sleeping]}";
        String result = new String();
        result += pet.getSpecies().species() + " {";
        result += "nickname: " + pet.getNickname();
        if (pet.getAge() == 0) {
        } else {
            result += ", age: " + pet.getAge();
        }
        result += ", it can fly: " + pet.getSpecies().canFly();
        result += ", it has fur: " + pet.getSpecies().hasFur();
        result += ", it has " + pet.getSpecies().numberOfLegs() + " legs";

        if (pet.getTrickLevel() == 0) {
        } else {
            result += ", trickLevel: " + pet.getTrickLevel();
        }
        if (pet.getHabits().length == 0) {
            result += " } ";
        } else {
            result += ", habits: " + Arrays.toString(pet.getHabits()) + "}";
        }
        //then
        assertEquals(expectedResult, result);
    }

    @Test
    public void testEqualsAndHashCodeForPetClass() {
        //given
        String[] petHabits = new String[]{"jumping", "scratching", "licking", "stealing food", "sleeping"};
        Pet pet1 = new Pet(Species.CAT, "Kotan", 7, 99, petHabits);
        Pet pet2 = new Pet(Species.CAT, "Kotan", 7, 99, petHabits);
        Pet pet3 = new Pet(Species.CAT, "Kotia", 7, 99, petHabits);
        //when
        //then
        assertEquals(pet1, pet2);
        assertNotEquals(pet1, pet3);
        assertNotEquals(pet2, pet3);
        assertTrue(pet1.hashCode() == pet2.hashCode());
        assertFalse(pet1.hashCode() == pet3.hashCode());
        assertFalse(pet2.hashCode() == pet3.hashCode());
    }

}
